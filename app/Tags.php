<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;

class Tags extends Model
{
  
  use CrudTrait;
  
  protected $table = 'tags';
  protected $fillable = ['name'];
  public $timestamps = true;
  
  public function posts() {
    
    return $this->belongsToMany(Post::class);
    
  }
  
  public function getRouteKeyName()
  {
    return 'name';
  }
  
}
