<?php

namespace App;


use Illuminate\Support\Facades\Auth;

use Carbon\Carbon;
use phpDocumentor\Reflection\DocBlock\Tag;
use Backpack\CRUD\CrudTrait;

class Post extends Model
{
  
  use CrudTrait;
  
  protected $table = 'posts';
  protected $fillable = ['title', 'description', 'body', 'user_id', 'category_id', 'imgUrl', 'videoUrl'];
  public $timestamps = true;
  
  public function comments() {
    
    return $this->hasMany(Comment::class);
    
  }
  
  public function user() {
    
    return $this->belongsTo(User::class);
    
  }
  
  public function addComment($body) {
    
//    $this->comments()->create(compact('body'));
    
    
    Comment::create([
      
      'body' => $body,
      'post_id' => $this->id,
      'user_id' => Auth::id()
      
    ]);
    
  }
  
  public function scopeFilter($query, $filters) {
      
    if ($month = $filters['month']) {
      $query->whereMonth('created_at', Carbon::parse($month)->month);
    }
  
    if ($year = $filters['year']) {
      $query->whereYear('created_at', $year);
    }
    
  }
  
  public static function archives() {
  
    return static::selectRaw('year(created_at) year, monthname(created_at) month, count(*) published')
      ->groupBy('year', 'month')
      ->orderByRaw('min(created_at) desc')
      ->get()
      ->toArray();
    
  }
  
  public function tags() {
    
    return $this->belongsToMany(Tags::class);
    
  }
  
  public function categories() {
    
    return $this->belongsTo(Category::class);
    
  }
  
}
