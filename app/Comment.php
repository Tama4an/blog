<?php

namespace App;
use Backpack\CRUD\CrudTrait;

class Comment extends Model
{
  
  use CrudTrait;
  
  protected $table = 'comments';
  protected $fillable = ['user_id', 'post_id', 'body'];
  public $timestamps = true;
  
  public function post() {
    
    return $this->belongsTo(Post::class);
    
  }
  
  public function user() {
    
    return $this->belongsTo(User::class);
    
  }
  
}
