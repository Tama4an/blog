<?php

namespace App\Http\Controllers;

use App\Tags;
use Illuminate\Http\Request;

use App\Post;
use App\Category;

use Carbon\Carbon;


class PostsController extends Controller
{
    
  public function __construct() {
    
    $this->middleware('auth')->except(['index', 'show']);
    
  }
  
  public function index() {
    
    $posts = Post::latest()
      ->filter(request(['month', 'year']))
      ->get();
        
    return view('posts.index', compact('posts'));
    
  }
  
  public function show($id) {
    
    $post = Post::find($id);
    
    return view('posts.show', compact('post'));
    
  }
  
  public function create() {
    
    $categories = Category::all();
    $tags = Tags::all();
    
    return view('posts.create', compact('categories', 'tags'));
    
  }
  
  public function store() {

    $this->validate(request(),[
      
      'title' => 'required',
      'body' => 'required'
      
    ]);
    //dd(request());
    $newPost = new Post(request(['title', 'body', 'description', 'category_id', 'imgUrl', 'videoUrl']));
    auth()->user()->publish($newPost);
    $tagsList = request('tags_id');
    $newPost->tags()->attach($tagsList);

    session()->flash('message', 'You post has now been published.');
    
    return redirect('/');
    
  }
  
  
  
}
