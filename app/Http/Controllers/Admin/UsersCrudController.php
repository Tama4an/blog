<?php namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;

use App\Http\Requests\UsersCrudRequest as StoreRequest;
use App\Http\Requests\UsersCrudRequest as UpdateRequest;

class UsersCrudController extends CrudController {
  
  public function setup() {
    $this->crud->setModel("App\User");
    $this->crud->setRoute("admin/user");
    $this->crud->setEntityNameStrings('users', 'User');
    
    $this->crud->setColumns(['name', 'email']);
    $this->crud->addFields([
      'name',
      'email',
    ]);
  }
  
  public function store(StoreRequest $request)
  {
    return parent::storeCrud();
  }
  
  public function update(UpdateRequest $request)
  {
    return parent::updateCrud();
  }
}