<?php namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;

use App\Http\Requests\PostCrudRequest as StoreRequest;
use App\Http\Requests\PostCrudRequest as UpdateRequest;

class PostsCrudController extends CrudController {
  
  public function setup() {
    $this->crud->setModel("App\Post");
    $this->crud->setRoute("admin/post");
    $this->crud->setEntityNameStrings('post', 'Posts');
    
    $this->crud->setColumns(['title', 'description', 'body', 'user_id', 'category_id']);
    $this->crud->addFields([
      'title',
      'description',
      'body',
      'user_id',
      'category_id'
    ]);
  }
  
  public function store(StoreRequest $request)
  {
    return parent::storeCrud();
  }
  
  public function update(UpdateRequest $request)
  {
    return parent::updateCrud();
  }
}

