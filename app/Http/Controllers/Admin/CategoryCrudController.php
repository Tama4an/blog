<?php namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;

use App\Http\Requests\CategoryCrudRequest as StoreRequest;
use App\Http\Requests\CategoryCrudRequest as UpdateRequest;

class CategoryCrudController extends CrudController {
  
  public function setup() {
    $this->crud->setModel("App\Category");
    $this->crud->setRoute("admin/category");
    $this->crud->setEntityNameStrings('category', 'Categories');
    
    $this->crud->setColumns(['name', 'description']);
    $this->crud->addFields([
      'name',
      'description'
    ]);
  }
  
  public function store(StoreRequest $request)
  {
    return parent::storeCrud();
  }
  
  public function update(UpdateRequest $request)
  {
    return parent::updateCrud();
  }
}