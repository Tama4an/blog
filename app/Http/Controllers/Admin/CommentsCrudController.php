<?php namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;

use App\Http\Requests\CommentsCrudRequest as StoreRequest;
use App\Http\Requests\CommentsCrudRequest as UpdateRequest;

class CommentsCrudController extends CrudController {
  
  public function setup() {
    $this->crud->setModel("App\Comment");
    $this->crud->setRoute("admin/comments");
    $this->crud->setEntityNameStrings('comment', 'Comments');
    
    $this->crud->setColumns(['user_id', 'post_id', 'body']);
    $this->crud->addFields([
      'user_id',
      'post_id',
      'body'
    ]);
  }
  
  public function store(StoreRequest $request)
  {
    return parent::storeCrud();
  }
  
  public function update(UpdateRequest $request)
  {
    return parent::updateCrud();
  }
}