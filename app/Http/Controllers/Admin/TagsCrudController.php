<?php namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;

use App\Http\Requests\UsersCrudRequest as StoreRequest;
use App\Http\Requests\UsersCrudRequest as UpdateRequest;

class TagsCrudController extends CrudController {
  
  public function setup() {
    $this->crud->setModel("App\Tags");
    $this->crud->setRoute("admin/tags");
    $this->crud->setEntityNameStrings('tags', 'Tags');
    
    $this->crud->setColumns(['name']);
    $this->crud->addFields([
      'name',
    ]);
  }
  
  public function store(StoreRequest $request)
  {
    return parent::storeCrud();
  }
  
  public function update(UpdateRequest $request)
  {
    return parent::updateCrud();
  }
}