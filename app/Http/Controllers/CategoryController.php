<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    
  public function index() {
    
    $categories = Category::all();
    return view('category.index', compact('categories'));
    
  }
  
  public function show(Category $category) {
    
    $posts = $category->posts;
    
    return view('posts.index', compact('posts'));
  }
  
}
