<?php

namespace App\Http\Controllers;

use App\Mail\Feedback;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class StaticController extends Controller
{
  
  public function __construct() {
    
    $this->middleware('auth');
    
  }
  
  public function contact() {
    return view('static.contacts');
  }
  
  public function about() {
    
    return view('static.about');
    
  }
  
  public function mailForm() {
    return view('static.feedback');
  }
  
  public function feedback() {
    
    $this->validate(request(), [
      'name' => 'required|string|',
      //'phone' => 'required|regex:"/[+][3][8][(][0-9]{3}[)][-][0-9]{3}[-][0-9]{2}[-][0-9]{2}$/i"',
      'email' => 'required|string|email',
      'body' => 'required|min:10'
    ],[
      'phone.regex' => 'ОШИБКА: введенный мобильный телефон не соответствует шаблону, пример: +38(xxx)-xxx-xx-xx '
    ]);
  
    if(!request('g-recaptcha-response')) {
      session()->flash('message', 'Заполните капчу');
      return back();
    } else {
      $url = 'https://www.google.com/recaptcha/api/siteverify';
      $key = '6LfUbiAUAAAAAE-QfJWufqKe7KpbzKKOpvxma4hM';
      $query = $url.'?secret='.$key.'&response='.request('g-recaptcha-response').'&remoteip='.$_SERVER['REMOTE_ADDR'];
      //$dghsdfg = file_get_contents($query);
      $capchaData = json_decode(file_get_contents($query), true);
      //dd($capchaData);
      if($capchaData['success'] != true) {
        session()->flash('message', 'Вы не прошли проверку и видимо являетесь роботом :)');
        return back();
      }
    
      \Mail::to('well4work@gmail.com', auth()->user()->name)->send(new Feedback, request());
      session()->flash('message', 'Сообщение успешно отправленно!');
      return view('static.contacts');
    }
    
  }
  
}
