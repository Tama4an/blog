<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Post;

class PanelController extends Controller
{
    
  public function __construct() {
    
    $this->middleware('auth');
    
  }
  
  public function index() {
        
    return view('panel.index');
    
  }
  
  public function showPosts() {
  
    $posts = auth()->user()->posts;
  
    return view('panel.show', compact('posts'));
    
  }
  
  public function name() {
    
    if (request('newName')) {
      auth()->user()->name = request('newName');
      auth()->user()->save();
      session()->flash('message', 'Имя пользователя успешно изменено!');
      return redirect('/panel');
    }
    return view('panel.name');
    
  }
    
  public function mail() {
    
    if (request('newEmail')) {
      auth()->user()->email = request('newEmail');
      auth()->user()->save();
      session()->flash('message', 'Электронный адресс пользователя успешно изменен!');
      return redirect('/panel');
    }
    
    return view('panel.mail');
    
  }
  
  public function password() {
    
    if (request('password')) {
      $this->validate(request(), [
        'password' => 'required|confirmed'
      ]);
      auth()->user()->password = bcrypt(request('password'));
      auth()->user()->save();
      session()->flash('message', 'Пароль пользователя успешно изменен!');
      return redirect('/panel');
    }
    return view('panel.password');
  }
  
  public function remove($id) {
    
    Post::destroy($id);
    session()->flash('message', 'Сообщение удалено!');
    return back();
  }
  
  public function edit($id) {
    
    $post = Post::find($id);
    if(!request('title')) {
      return view('panel.editPost', compact('post'));
    }
    $post->update([
      'title' => request('title'),
      'description' => request('description'),
      'imgUrl' => request('imgUrl'),
      'videoUrl' => request('videoUrl'),
      'body' => request('body')
    ]);
    session()->flash('message', 'Изменения успешно сохранены!');
    return redirect('/panel/posts');
    
  }
  
}
