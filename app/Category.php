<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;

class Category extends Model
{
  
  use CrudTrait;
  
  protected $table = 'categories';
  protected $fillable = ['name', 'description'];
  public $timestamps = true;
  
  public function posts() {
    
    return $this->hasMany(Post::class);
    
  }
  
  public function getRouteKeyName()
  {
    return 'name';
  }
  
}
