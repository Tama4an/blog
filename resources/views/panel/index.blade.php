@extends('panel.master')


@section('content')

    <div class="col-sm-8 blog-main">

        <h4>Меню</h4>
        <ol class="list-unstyled">
            <li>
                <a href="/panel/posts">Посты пользователя</a>
            </li>
            <li>
                <a href="/panel/password">Смена пароля</a>
            </li>
            <li>
                <a href="/panel/mail">Смена электронной почты</a>
            </li>
            <li>
                <a href="/panel/name">Смена имени пользователя</a>
            </li>
        </ol>

        <nav class="blog-pagination">
            <a class="btn btn-outline-primary" href="#">Older</a>
            <a class="btn btn-outline-secondary disabled" href="#">Newer</a>
        </nav>

    </div>

@endsection
