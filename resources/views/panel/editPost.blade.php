@extends('layouts.master')

@section('content')

    <div class="col-sm-8 blog-main">

        <h1>Редактировать пост</h1>

        <hr>

        <form method="POST" action="/panel/posts/{{ $post->id }}/edit">

            {{ csrf_field() }}

            <div class="form-group">

                <label for="title">Заголовок:</label>

                <input type="text" class="form-control" id="title" name="title" value="{{ $post->title }}">

            </div>

            <div class="form-group">

                <label for="description">Описание:</label>

                <input type="text" class="form-control" id="description" name="description" value="{{ $post->description }}">

            </div>

            <div class="form-group">

                <label for="imgUrl">Ссылка на картинку:</label>

                <input type="text" class="form-control" id="imgUrl" name="imgUrl" value="{{ $post->imgUrl }}">

            </div>

            <div class="form-group">

                <label for="videoUrl">Ссылка на видео:</label>

                <input type="text" class="form-control" id="videoUrl" name="videoUrl" value="{{ $post->videoUrl }}">

            </div>

            <div class="form-group">

                <label for="body">Текст поста:</label>

                <textarea name="body" id="body" class="form-control">{{ $post->body }}</textarea>

            </div>

            {{--<div class="form-group">--}}

                {{--<label for="tag">Тег:</label>--}}
                {{--<select name="tags_id[]" multiple>--}}
                    {{--@foreach($tags as $tag)--}}
                        {{--<option value="{{ $tag->id }}">{{ $tag->name }}</option>--}}
                    {{--@endforeach--}}
                {{--</select>--}}

                {{--<label for="category_id">Категория:</label>--}}
                {{--<select name="category_id">--}}
                    {{--@foreach($categories as $category)--}}
                        {{--<option value={{ $category->id }}>{{ $category->name }}</option>--}}
                    {{--@endforeach--}}
                {{--</select>--}}
            {{--</div>--}}

            <div class="form-group">

                <button type="submit" class="btn btn-primary">Опубликовать</button>

            </div>

            @include('layouts.errors')

        </form>

    </div>

@endsection
