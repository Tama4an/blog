<div class="col-sm-3 offset-sm-1 blog-sidebar">
    <div class="sidebar-module sidebar-module-inset">
        <a href="/panel"><h4>Меню</h4></a>
        <ol class="list-unstyled">
            <li>
                <a href="/panel/posts">Посты пользователя</a>
            </li>
            <li>
                <a href="/panel/password">Смена пароля</a>
            </li>
            <li>
                <a href="/panel/mail">Смена электронной почты</a>
            </li>
            <li>
                <a href="/panel/name">Смена имени пользователя</a>
            </li>
        </ol>
    </div>
</div>