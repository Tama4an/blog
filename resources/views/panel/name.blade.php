@extends('panel.master')


@section('content')

    <div class="col-sm-8 blog-main">
        <h1>Сменить имя</h1>
        <hr>
        <form method="POST" action="/panel/name">
            {{ csrf_field() }}
            <div class="form-group">

                <label for="newName">Новое имя пользователя:</label>

                <input type="text" class="form-control" id="newName" name="newName">

            </div>

            <div class="form-group">

                <button type="submit" class="btn btn-primary">Изменить</button>

            </div>

            <nav class="blog-pagination">
                <a class="btn btn-outline-primary" href="#">Older</a>
                <a class="btn btn-outline-secondary disabled" href="#">Newer</a>
            </nav>
        </form>
    </div>

@endsection
