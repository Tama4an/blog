
    @extends('layouts.master')

    @section('content')

        <div class="col-sm-8 blog-main">

            <h1>{{ $post->title }}</h1>

            <p>{{ $post->user->name }} on {{ $post->created_at->diffForHumans() }}</p>
            <hr>

            @if(count($post->tags))

                <ul>
                    @foreach($post->tags as $tag)
                        <li>
                            <a href="/tags/{{ $tag->name }}">{{ $tag->name }}</a></li>
                    @endforeach
                </ul>

            @endif
            <hr>

            @if($post->imgUrl)
                <img src="{{ $post->imgUrl }}">
            @endif
            <br>

            @if($post->videoUrl)
                <?php
                    $parsedUrl = parse_url($post->videoUrl);
                    parse_str($parsedUrl['query'], $parsedQuery);
                    $url = 'http://www.youtube.com/embed/'.$parsedQuery['v'];
                ?>

                <iframe width="854" height="480" src="{{ $url }}" frameborder="0" allowfullscreen></iframe>

            @endif

            {{ $post->body }}

            <hr>

            <div class="comments">

                <ul class="list-group">

                    @foreach($post->comments as $comment)

                        <li class="list-group-item">

                            <strong>

                                {{ $comment->user->name }} on
                                {{ $comment->created_at->diffForHumans() }}: &nbsp;

                            </strong>

                            {{ $comment->body }}

                        </li>

                    @endforeach

                </ul>

            </div>

            {{-- Add a coment --}}

            <hr>

            <div class="card">

                <div class="card-block">

                    <form method="POST" action="/posts/{{ $post->id }}/comments">

                        {{ csrf_field() }}

                        <div class="form-group">
                            <textarea name="body" placeholder="Введите текст сообщения." class="form-control"></textarea>
                        </div>

                        <div class="form-group">
                            <button type="submit" class="btn btn-primary">Опубликовать</button>
                        </div>

                    </form>

                    @include('layouts.errors')

                </div>

            </div>

        </div>

    @endsection