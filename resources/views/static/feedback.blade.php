@extends('layouts.master')

@section('content')

    <div class="col-sm-8 blog-main">

        <h1>Написать нам</h1>
        <hr>

        <form method="POST" action="/contact/mail">

            {{ csrf_field() }}

            <div class="form-group">

                <label for="name">ФИО:</label>

                <input type="text" class="form-control" id="name" name="name">

            </div>

            <div class="form-group">

                <label for="phone">Телефон:</label>

                <input type="text" class="form-control" id="phone" name="phone" pattern="+38([0-9]{3})-[0-9]{3}-[0-9]{2}-[0-9]{2}" placeholder="+38(xxx)-xxx-xx-xx">

            </div>

            <div class="form-group">

                <label for="email">Почта:</label>

                <input type="text" class="form-control" id="email" name="email">

            </div>

            <div class="form-group">

                <label for="body">Сообщение:</label>

                <textarea name="body" id="body" class="form-control"></textarea>

            </div>

            <div class="g-recaptcha" data-sitekey="6LfUbiAUAAAAAMeiDHYXsmz8WoHceZWPahGjBKTt"></div>

            <div class="form-group">

                <button type="submit" class="btn btn-primary">Отправить</button>

            </div>

            @include('layouts.errors')

        </form>

    </div>

@endsection