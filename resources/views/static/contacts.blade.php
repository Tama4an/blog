@extends('layouts.master')

@section('content')

    <div class="col-sm-8 blog-main">

        <h1>Наши контакты</h1>

        <hr>

        <p>Phone: +38(095)2401411</p>
        <p>Email: well4work@gmail.com</p>
        <a class="btn btn-outline-primary" href="/contact/mail">Написать нам</a>


            @include('layouts.errors')

        </form>

    </div>

@endsection