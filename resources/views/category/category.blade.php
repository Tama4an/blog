<div class="blog-post">
    <h2 class="blog-post-title">

        <a href="/category/{{ $category->name }}">
            {{ $category->name }}
        </a>

    </h2>

    {{--<p class="blog-post-meta">--}}
        {{--{{ $category->created_at->toFormattedDateString() }}--}}
    {{--</p>--}}
    {{ $category->description }}
</div>