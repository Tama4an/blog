
@extends('layouts.master')

@section('content')

    <div class="col-sm-8 blog-main">

        <h1>{{ $category->name }}</h1>

        {{ $category->description }}

    </div>

@endsection