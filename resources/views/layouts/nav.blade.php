<div class="blog-masthead">
    <div class="container">
        <nav class="nav blog-nav">
            <a class="nav-link active" href="/">Главная</a>
            <a class="nav-link" href="/category">Категории</a>
            <a class="nav-link" href="/posts/create">Добавить пост</a>
            <a class="nav-link" href="/contact">Контакты</a>
            <a class="nav-link" href="/about">О нас</a>
            @if(!Auth::guest())
                <a class="nav-link" href="/panel">Личный кабинет</a>
            @endif

            <div class="top-right links" display="inline">
                @if (Auth::guest())
                    <a href="{{ route('login') }}">Login</a>
                    <a href="{{ route('register') }}">Register</a>
                @else

                    <a href="{{ route('logout') }}"
                       onclick="event.preventDefault();
                       document.getElementById('logout-form').submit();">
                        Logout
                    </a>

                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        {{ csrf_field() }}
                    </form>

                    <a href="/panel">{{ Auth::user()->name }}</a>
                @endif
            </div>

        </nav>
    </div>
</div>
