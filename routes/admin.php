<?php

CRUD::resource('category', 'CategoryCrudController');
CRUD::resource('comments', 'CommentsCrudController');
CRUD::resource('post', 'PostsCrudController');
CRUD::resource('tags', 'TagsCrudController');
CRUD::resource('user', 'UsersCrudController');