<?php

Route::get('/', 'PostsController@index')->name('home');
Route::get('/posts/create', 'PostsController@create');
Route::get('/posts/{post}', 'PostsController@show');
Route::post('/posts', 'PostsController@store');
Route::post('/posts/{post}/comments', 'CommentsController@store');
Route::get('/about', 'StaticController@about');
Route::get('/contact', 'StaticController@contact');
Route::get('/contact/mail', 'StaticController@mailForm');
Route::post('/contact/mail', 'StaticController@feedback');

Auth::routes();

Route::get('/home', 'PostsController@index');
Route::get('/tags/{tag}', 'TagsController@index');
Route::get('/category', 'CategoryController@index');
Route::get('/category/{category}', 'CategoryController@show');

Route::get('/panel', 'PanelController@index');
Route::get('/panel/posts', 'PanelController@showPosts');
Route::get('/panel/name', 'PanelController@name');
Route::post('/panel/name', 'PanelController@name');
Route::get('/panel/mail', 'PanelController@mail');
Route::post('/panel/mail', 'PanelController@mail');
Route::get('/panel/password', 'PanelController@password');
Route::post('/panel/password', 'PanelController@password');

Route::get('/panel/posts/{post}/delete', 'PanelController@remove');
Route::get('/panel/posts/{post}/edit', 'PanelController@edit');
Route::post('/panel/posts/{post}/edit', 'PanelController@edit');